﻿using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace PhotoUploadTest.Controllers
{
    public class Request
    {
        public string Content { get; set; }
    }

    public class UploadController : ApiController
    {
        [Route("api/UploadBase64")]
        [HttpPost]
        public IHttpActionResult UploadBase64([FromBody]Request request)
        {
            byte[] bytes = Convert.FromBase64String(request.Content);

            return Ok();
        }

        [Route("api/UploadUrlEncoded")]
        [HttpPost]
        public IHttpActionResult UploadUrlEncoded(FormDataCollection collection)
        {
            byte[] bytes = HttpUtility.UrlDecodeToBytes(collection["file"]);

            return Ok();
        }

        [Route("api/UploadMultipart")]
        [HttpPost]
        public async Task<IHttpActionResult> UploadMultipart(HttpRequestMessage request)
        {
            var multipartMemoryStreamProvider = await request.Content.ReadAsMultipartAsync();
            var httpContent = multipartMemoryStreamProvider.Contents[0];
            var bytes = await httpContent.ReadAsByteArrayAsync();

            return Ok();
        }
    }
}